require 'tickbot_subscriber/version'
require 'tickbot_subscriber/configuration'
require 'tickbot_subscriber/client'

module TickbotSubscriber
  class Error < StandardError; end
end
