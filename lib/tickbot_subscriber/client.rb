require 'singleton'
require 'bunny'
module TickbotSubscriber
  class Client
    include Singleton
    attr_accessor :config, :channel, :queue, :client
    class << self
      def configuration
        @configuration || TickbotSubscriber::Configuration.new
      end

      def configure
        yield configuration
      end

      def reset
        @configuration = TickbotSubscriber::Configuration.new
      end
    end

    def initialize(config = TickbotSubscriber::Configuration.new)
      @config = config
    end

    def connection
      @connection ||= Bunny.new(port: config.port, host: config.host, username: config.user, pass: config.pass)
      @connection.start
      @connection
    end

    def channel
      @channel ||= connection.create_channel
      @channel.prefetch(2)
      @channel
    end

    def inbox_queue
      queue('tickbot.inbox')
    end

    def broadcast_queue
      queue('tickbot.broadcast')
    end

    def queue(queue_name)
      channel.queue(queue_name)
    end

    def client(name, &block)
      begin
        queue(name).subscribe(block: true) do |_delivery_info, _properties, body|
          yield body
        end
      rescue Interrupt => _
        connection.close
      end
    end
  end
end
