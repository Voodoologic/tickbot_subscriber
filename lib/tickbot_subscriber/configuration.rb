require 'yaml'
module TickbotSubscriber
  class Configuration
    attr_accessor :port, :host, :user, :pass
    def initialize
      get_defaults
    end

    def get_defaults
      secrets = Pathname.pwd + '.secrets.yml'
      if secrets.exist?
        secrets_config = YAML.load_file(secrets)
        @port  = secrets_config.fetch('rabbitmq_port')
        @host  = secrets_config.fetch('rabbitmq_host')
        @user  = secrets_config.fetch('rabbitmq_user')
        @pass  = secrets_config.fetch('rabbitmq_pass')
      else
        @port = ENV.fetch('RABBITMQ_PORT')
        @host = ENV.fetch('RABBITMQ_HOST')
        @user = ENV.fetch('RABBITMQ_USER')
        @pass = ENV.fetch('RABBITMQ_PASS')
      end
    end
  end
end
